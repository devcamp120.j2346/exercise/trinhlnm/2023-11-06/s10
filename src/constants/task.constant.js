export const INPUT_TASK_CHANGE = "INPUT Task Change";

export const BUTTON_ADD_TASK_CLICKED = "Button add task clicked";

export const TASK_TOGGLE_CLICKED = "Task toggle clicked";